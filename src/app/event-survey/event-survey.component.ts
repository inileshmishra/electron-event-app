import { Component, OnInit } from '@angular/core';
import {ServiceService} from '../dataservice/service.service'
import PouchdbFind from 'pouchdb-find';
import {Subject, BehaviorSubject } from 'rxjs';
import { Router ,NavigationEnd} from '@angular/router';

declare var require: any
var PouchDB = require('pouchdb').default;
// var db = new PouchDB('events-nilesh');
declare var $:any;
PouchDB.plugin(require('pouchdb-find'));

// var remoteDB = new PouchDB('http://app:d0cqu1ty@ec2-52-74-131-38.ap-southeast-1.compute.amazonaws.com:5984/event_1');

var userdetail = new Subject <any>();

var submitsuccess= new Subject<any>();

@Component({
  selector: 'app-event-survey',
  templateUrl: './event-survey.component.html',
  styleUrls: ['./event-survey.component.css']
})
export class EventSurveyComponent implements OnInit {

  fname:any;
  lname:any;
  regnumber:any;
  email:any;
  mnumber:any;
  questionone;
  questiontwo;
  questionthree;
  questionfour;
  questionfive;
  questionsix;
  comment;
  survey;

  gridRadios1:any;

  errquestionone:any;
  errquestiontwo:any;
  errquestionthree:any;
  errquestionfour:any;
  errquestionfive:any;
  errquestionsix:any;
  localdb:any;
  remoteDB:any;
  

  constructor(private service:ServiceService,private router:Router ) {
   
    this.connectDB();
    this.mnumber=this.service.mnumber;
    console.log(this.mnumber)
    PouchDB.plugin(PouchdbFind);

    //subscribe userdetail data
    userdetail.subscribe(data=>{

      this.fname=data.f_name;
      this.lname=data.l_name;
      this.regnumber=data.regnumber;
      this.email=data.email;
      this.mnumber=data.mob_number;
      console.log(this.fname)
    })

    submitsuccess.subscribe(data=>{
      this.syncdb();
    })
   }


   connectDB(){
    
    let dbname=this.service.database;
     this.localdb = new PouchDB(dbname);
     this.service.localdb=this.localdb;
     console.log(this.localdb.name)

      this.remoteDB = new PouchDB('http://app:d0cqu1ty@ec2-52-74-131-38.ap-southeast-1.compute.amazonaws.com:5984/'+dbname);

      this.syncdb();
 }

  ngOnInit() {

    if (this.service.database == undefined ||this.service.database == ''){
      this.router.navigateByUrl('/')
      }

    this.getuserdetail();

    
  }

  getuserdetail(){
    this.localdb.get(this.service.mnumber).then(function (todo) {
      console.log(todo);
     userdetail.next(todo);
    }).catch(function (err) {
      // punchoutnotfound.next("");
    });
  }


  selectq1(value,$event){
    console.log(value)
    console.log($event.srcElement.defaultValue)
    this.questionone=$event.srcElement.defaultValue;
}

selectq2(value,$event){
  console.log(value)
  console.log($event.srcElement.defaultValue)
  this.questiontwo=$event.srcElement.defaultValue;
}

selectq3(value,$event){
console.log(value)
console.log($event.srcElement.defaultValue)
this.questionthree=$event.srcElement.defaultValue;
}

selectq4(value,$event){
console.log(value)
console.log($event.srcElement.defaultValue)
this.questionfour=$event.srcElement.defaultValue;
}

selectq5(value,$event){
console.log(value)
console.log($event.srcElement.defaultValue)
this.questionfive=$event.srcElement.defaultValue;
}

selectq6(value,$event){
console.log(value)
console.log($event.srcElement.defaultValue)
this.questionsix=$event.srcElement.defaultValue;
}



submitForm(){ 

  // this.comment=document.getElementById("exampleFormControlTextarea1").value;
  this.comment=$("#exampleFormControlTextarea1").val();
  

   if(this.questionone=='' || this.questionone==undefined){
     this.errquestionone="Please select one option."
     return;
      }

      if(this.questiontwo=='' || this.questiontwo==undefined){
        this.errquestiontwo="Please select one option."
            return;
        }

        if(this.questionthree=='' || this.questionthree==undefined){
          this.errquestionthree="Please select one option."
          return;
      }

      if(this.questionfour=='' || this.questionfour==undefined){
        this.errquestionfour="Please select one option."
        return;
      }

      if(this.questionfive=='' || this.questionfive==undefined){
        this.errquestionfive="Please select one option."
        return;
      }

      if(this.questionsix=='' || this.questionsix==undefined){
        this.errquestionsix="Please select one option."
        return;
      }

     
      let dt=new Date();
          
      this.survey = {
        key:'survey',
        _id:this.mnumber+'survey',
        firstname: this.fname,
        lastname:this.lname,
        mobnumber:this.mnumber,
        questionone:this.questionone,
        questiontwo:this.questiontwo,
        questionthree:this.questionthree,
        questionfour:this.questionfour,
        questionfive:this.questionfive,
        questionsix:this.questionsix,
        comment:this.comment
      };
      console.log(this.survey)
      this.localdb.put(this.survey, function callback(err, result) {
        if (!err) 
        {
          //Success Variable Boolean
           submitsuccess.next(true);
          console.log('Successfully posted a todo!');       
        }
        else{
          console.log(err)
        }
      });

      this.router.navigateByUrl('punchin');

 }


  //sync database
  syncdb(){ console.log(this.service.database)
   
    this.service.localdb.sync(this.remoteDB).on('complete', function () {
      // yay, we're in sync!
    }).on('error', function (err) {
      // boo, we hit an error!
      console.log(err)
    });
  }






}
