import { Component, OnInit, ɵConsole } from '@angular/core';
import PouchdbFind from 'pouchdb-find';
import {Subject, BehaviorSubject } from 'rxjs';
import { Router ,NavigationEnd} from '@angular/router';
import { ExportToCsv } from 'export-to-csv';

declare var require: any
var PouchDB = require('pouchdb').default;

// var remoteDB = new PouchDB('http://app:d0cqu1ty@ec2-52-74-131-38.ap-southeast-1.compute.amazonaws.com:5984/'+this.localdb)
import {ServiceService} from '../dataservice/service.service'
PouchDB.plugin(require('pouchdb-find'));
declare var $ : any;

var submitsuccess = new Subject<any>();

var openform= new Subject<any>();

var punchout= new Subject<any>();

var punchoutnotfound =new Subject<any>();

var punchoutsuccess=new Subject<any>();

var userspecilaity= new Subject<any>();

var download=new Subject<any>();

var downloadsurvey= new Subject<any>();



@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  fname:any;
  lname:any;
  mnumber:string;
  event:any;
  regnumber:any;
  showColor:boolean=false;
  showsuccess:boolean=false;
  openFormdiv:boolean=false;
  email:any;
  todo:any;
  savaechanges:boolean=false;
  punchoutnotfound:boolean=false;
  succespunchout:boolean=false;
  speciality:any;
  selected_speciality:any;
  eventname:any;
  event_id:any;
  country_code:any;
  localdb:any;
  remoteDB:any;




  constructor(public SerService:ServiceService, public router:Router) {  

    if (this.SerService.event == undefined || this.SerService.event == ''){
      this.router.navigateByUrl('/')
      }
    //connectodb
    this.connectDB();
    
    PouchDB.plugin(PouchdbFind);

    submitsuccess.subscribe(data=>{
      this.mnumber='';
   
      this.lname='';
      this.mnumber='';
      this.email='';
      this.regnumber='';
       this.selected_speciality='';
      this.showsuccess=true;
      this.openFormdiv=false;
      this.savaechanges=false;
      this.showColor=false;
      
      
      setTimeout(() => {
        this.showsuccess=false;
        this.fname='';
      }, 4000);

      this.syncdb();
    })

    openform.subscribe(data=>{
    this.openFormdiv=true;
    })

    punchoutnotfound.subscribe(data=>{
        this.punchoutnotfound=true;
        setTimeout(() => {
          this.punchoutnotfound=false;
        }, 5000);
    })

    punchoutsuccess.subscribe(data=>{ 
      this.succespunchout=true;
      this.mnumber='';
     
      this.lname='';
      this.mnumber='';
      this.email='';
      this.regnumber='';
       this.selected_speciality='';
      // this.showsuccess=true;
      this.openFormdiv=false;
      this.showColor=false;
      this.savaechanges=false;
      setTimeout(() => {
        this.succespunchout=false;
        this.fname='';
      }, 5000);

      this.syncdb();
      this.router.navigateByUrl('survey')
    })

    // userspecilaity.subscribe(data=>{
     this.speciality=this.SerService.speciality;
    // })

    
    download.subscribe(result=>{
      let dt=new Date();
      let options = { 
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: true, 
        showTitle: true,
        title: this.SerService.event+'_User Info_'+dt,
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
       // headers: ['Column 1', 'Column 2', 'c3',"c4","c5","c6","c7"] 
      };
      
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(result.docs); 
    })

    downloadsurvey.subscribe(result=>{
      let dt=new Date();
      let options = { 
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: true, 
        showTitle: true,
        title: this.SerService.event+'_Survey_'+dt,
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
       // headers: ['Column 1', 'Column 2', 'c3',"c4","c5","c6","c7"] 
      };
      const csvExporter = new ExportToCsv(options);
      csvExporter.generateCsv(result.docs); 
    })




   }

   connectDB(){
      let dbname=this.SerService.database;
       this.localdb = new PouchDB(dbname);
       console.log(this.localdb.name)

        this.remoteDB = new PouchDB('http://app:d0cqu1ty@ec2-52-74-131-38.ap-southeast-1.compute.amazonaws.com:5984/'+dbname)
   }

  ngOnInit() {

    

    this.eventname=this.SerService.event;
    this.event_id=this.SerService.event_id;
    this.country_code=this.SerService.country_id;

   
    
       //get specility function
       this.getspeciality();

     
  }

  
  //download data
  handleExport() {

    this.syncdb();

    this.localdb.find({
      selector: {key_category: 'userdata'},
     
     
    }).then(function (result) {
      console.log(result)

      download.next(result);  

    }).catch(function (err) {
      console.log(err);
    });



  //   this.syncdb();
  //   this.localdb.allDocs({include_docs: true}, (error, doc) => {

      
  //     if (error) console.error(error);
  //     else 
  //      { JSON.stringify(doc.rows.map(({doc}) => doc)),
  //       'tracker.db',
  //       'text/plain',
  //     console.log(doc)


  //     for(var i = 0; i < doc.rows.length; i++) {
  //       if(doc.rows[i].doc.email !='' && doc.rows[i].doc.email !=undefined){

  //               doc.rows[i]['fname']=doc.rows[i].doc.f_name;
  //               doc.rows[i]['lname']=doc.rows[i].doc.l_name;
  //               doc.rows[i]['mnumber']=doc.rows[i].doc.mob_number;
  //               doc.rows[i]['event']=doc.rows[i].doc.event;
  //               doc.rows[i]['email']=doc.rows[i].doc.email;
  //               doc.rows[i]['regnumber']=doc.rows[i].doc.regnumber;
  //               doc.rows[i]['spec']=doc.rows[i].doc.speciality;
  //               doc.rows[i]['punchtime']=doc.rows[i].doc.punchin_time;
  //               doc.rows[i]['fnampunchoute']=doc.rows[i].doc.punch_out;

  //               delete doc.rows[i]['value'];
  //               delete doc.rows[i]['doc'];   
  //   }
  //   }

  //   console.log(doc.rows);
  // //  csvExporter.generateCsv((doc.rows));    
    
  //   }
      
  //   });
  }

  //Update by _id
  updateUserInfomation(userId) {
  
  }
  

  addUserInformation() {
    let dt=new Date();
     
    this.todo = {
      key_category:'userdata',
      _id: this.mnumber,
      f_name: this.fname,
      l_name:this.lname,
      mob_number:this.mnumber,
      event:this.eventname,
      event_id:this.event_id,
      country_code:this.country_code,
      email:this.email,
      regnumber: this.regnumber,
      speciality:this.selected_speciality,
      punchin_time:dt.getTime(),
      punch_out:'',

      questionone:''
    };

    this.localdb.put(this.todo, function callback(err, result) {
      if (!err) 
      {
        //Success Variable Boolean
        submitsuccess.next(true);
        console.log('Successfully posted a todo!');
      }
    });
  }




  showtodo() {
    this.localdb.allDocs({include_docs: true, descending: true}, function(err, doc) {
     console.log(doc.rows);
    });
  }

  // punchin click
  punchin(){ 
    this.localdb.find({
      selector: {mob_number:this.mnumber},
    }).then(function (result) {

      if(result.docs.length >0){
        submitsuccess.next(1);
      }
      else{
        openform.next("openform");
      }
      console.log(result)
    }).catch(function (err) {
      console.log(err);
    });
  }


  punchout(){
    let loDb=this.localdb;
    this.localdb.get(this.mnumber).then(function (todo) { 
     console.log(todo)
      let dt=new Date();
      todo.punch_out = dt.getTime();
      console.log(todo);
      loDb.put(todo);
      punchoutsuccess.next("");
    }).catch(function (err) {
      console.log(err)
      punchoutnotfound.next("");
    });
    this.SerService.mnumber=this.mnumber;
  }

  //mobile number validation
  InputvalidateNo(event){  
   // remover zero if it is at first position
    if(this.mnumber.charAt(0)=='0'){
      this.mnumber='';
    }
    //length condition on mobile number
    if(this.mnumber.length >4 ){
      event.target.value=event.target.value.substr(0,12);
      this.showColor=true;
    }
    else{
      this.showColor=false;
    }
  }

  //mobile number validation only allow digit
  numberInput(e)
  {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
             return false;
    }
  }

//user form validation
  Inputvaliduserform(e){
     if( this.fname.length > 3 && this.regnumber.length >2 &&this.lname.length>3 && this.email.length >5) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      let x= regex.test(this.email);
      if(x==true){
        this.savaechanges=true;
      }
     }
     else{
      this.savaechanges=false;
     }
     
  }

  //sync database
  syncdb(){
    this.localdb.sync(this.remoteDB).on('complete', function () {
      // yay, we're in sync!
    }).on('error', function (err) {
      // boo, we hit an error!
      console.log(err)
    });
  }

  //get specilaity
    getspeciality(){
      this.localdb.get('speciality').then(function (todo) {  
        setTimeout(() => {
        userspecilaity.next(todo);
        }, 500);
      }).catch(function (err) {
      
      });
      
    }

    //select specility
    onChange(speci)
    { 
        this.selected_speciality=speci;
    }




    downloadsdurvey(){
      this.syncdb();
     
      this.localdb.find({
        selector: {key: 'survey'},
       
       
      }).then(function (result) {
        console.log(result)
  
        downloadsurvey.next(result);  
  
      }).catch(function (err) {
        console.log(err);
      });
    }




 

}
