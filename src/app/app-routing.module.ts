import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LoginEventComponent } from './login-event/login-event.component';
import { UserFormComponent } from './user-form/user-form.component';
import { EventSurveyComponent } from './event-survey/event-survey.component';

const webRoutes: Routes = [
    { path:'',component:LoginEventComponent },
    { path:'punchin',component:UserFormComponent },
    { path:'survey',component:EventSurveyComponent },
]




@NgModule({
    imports: [
    CommonModule,
    RouterModule.forRoot(webRoutes),
    ],
    exports: [RouterModule],
    declarations: []
    })
    export class AppRoutingModule { }