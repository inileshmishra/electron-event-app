import { Component, OnInit } from '@angular/core';
import {ServiceService} from '../dataservice/service.service'
import PouchdbFind from 'pouchdb-find';
import {Subject, BehaviorSubject } from 'rxjs';
import { Router ,NavigationEnd} from '@angular/router';
declare var require: any
var PouchDB = require('pouchdb').default;
var masterDB = new PouchDB('events-master');
PouchDB.plugin(require('pouchdb-find'));

var remoteDB = new PouchDB('http://app:d0cqu1ty@ec2-52-74-131-38.ap-southeast-1.compute.amazonaws.com:5984/master');

var logincheck = new Subject<any>();

var geteventlist = new Subject<any>();

var loginerror= new Subject<any>();

var eventdata= new Subject<any>()

var userspecilaity= new Subject<any>();


@Component({
  selector: 'app-login-event',
  templateUrl: './login-event.component.html',
  styleUrls: ['./login-event.component.css']
})
export class LoginEventComponent implements OnInit {

  username:any;
  password:any;
  eventlistarr:any;
  selected_event:any;
  savaechanges:boolean=false;
  usernameerror:boolean=false;
  passworderror:boolean=false;
  eventerror:boolean=false;
  loginerror:boolean=false;

  constructor( public bycryptService:ServiceService,   private router  : Router,) {
    
    PouchDB.plugin(PouchdbFind);

    logincheck.subscribe(data=>{
      if(this.selected_event.length >0){
          this.router.navigateByUrl('/punchin')
      }
    
    })


    geteventlist.subscribe(data=>{
      this.eventlistarr=data;
   
    })


    loginerror.subscribe(data=>{
      this.loginerror=true;
    })

    //event data
    eventdata.subscribe(data=>{
    
      this.bycryptService.event_id=data.docs[0].event_id;
      this.bycryptService.database=data.docs[0].database;
      this.bycryptService.country_id=data.docs[0].country_code;
      console.log( this.bycryptService.database)
    })

    userspecilaity.subscribe(data=>{
      this.bycryptService.speciality=data.speciality;

      console.log(this.bycryptService)
     })
  
  }

  ngOnInit() {
    setTimeout(() => {
      this.syncdb();
      this.geteventlist();
      this.getspeciality();
    masterDB.allDocs({include_docs: true, descending: true}, function(err, doc) {
     console.log(doc.rows);
    });
  
     
    }, 1000);
  }

  login(){  

    if(this.username==undefined ||  this.username.length<3   ){
      this.usernameerror=true;
      return;
    }
    else if( this.password==undefined || this.password.length<3 ){
      this.passworderror=true;
      return false;
    }
    else if(this.selected_event==undefined || this.selected_event.length<1 ){
   
      this.eventerror=true;
      return false;
    }
    this.checkuser()
  }

  geteventlist(){
    masterDB.find({
      selector: {key_category:'events'},
    }).then(function (result) {
      console.log(result)
      if(result.docs.length>0){
        geteventlist.next(result.docs);
      }
      else{
      }  
    }).catch(function (err) {
      console.log(err);
    });
  }

  checkuser(){
    masterDB.find({
      selector: {key_category:'authorization', username:this.username,password:this.password},
    }).then(function (result) {
      if(result.docs.length>0){
      logincheck.next();
      }
      else{
        loginerror.next();
      }
      
    }).catch(function (err) {
      console.log(err);
    });
  }


  //select event
  onChange(event)
  {  
    masterDB.find({
      selector: {key_category:'events', event_name:event},
    }).then(function (result) {
      eventdata.next(result);
    }).catch(function (err) {
      console.log(err);
    });

      this.eventerror=false;
      this.selected_event=event;
      this.bycryptService.event=event;
      
  
  }


   //sync database
   syncdb(){
    masterDB.sync(remoteDB).on('complete', function () {
      // yay, we're in sync!
    }).on('error', function (err) {
      // boo, we hit an error!
      console.log(err)
    });
  }

  usernamekeyup(e){
    this.usernameerror=false;
    this.loginerror=false;
  }

  passwordkeyup(e){
    this.passworderror=false;
    this.loginerror=false;
  }


   //get specilaity
   getspeciality(){
    masterDB.get('speciality').then(function (todo) {  
      setTimeout(() => {
      userspecilaity.next(todo);
      }, 500);
    }).catch(function (err) {
    
    });
    
  }



}
